# Terraform providers

This repo contains a terraform module with 2 alias providers. These providers are expected to be passed implicidly. This works perfectly in Terraform .11.

What am I doing wrong?

Init seems yust fine:
```text
[0m[1mInitializing modules...[0m
- bla in bla

[0m[1mInitializing the backend...[0m

[0m[1mInitializing provider plugins...[0m
- Checking for available provider plugins...
- Downloading plugin for provider "aws" (terraform-providers/aws) 2.22.0...

The following providers do not have any version constraints in configuration,
so the latest version was installed.

To prevent automatic upgrades to new major versions that may contain breaking
changes, it is recommended to add version = "..." constraints to the
corresponding provider blocks in configuration, with the constraint strings
suggested below.

* provider.aws: version = "~> 2.22"

[0m[1m[32mTerraform has been successfully initialized![0m[32m[0m
[0m[32m
You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.[0m
```

apply gives errors.

```text
[31m
[1m[31mError: [0m[0m[1mProvider configuration not present[0m

[0mTo work with module.bla.data.aws_caller_identity.management its original
provider configuration at module.bla.provider.aws.management is required, but
it has been removed. This occurs when a provider configuration is removed
while objects created by that provider still exist in the state. Re-add the
provider configuration to destroy
module.bla.data.aws_caller_identity.management, after which you can remove the
provider configuration again.
[0m[0m
[31m
[1m[31mError: [0m[0m[1mProvider configuration not present[0m

[0mTo work with module.bla.data.aws_caller_identity.logs its original provider
configuration at module.bla.provider.aws.logs is required, but it has been
removed. This occurs when a provider configuration is removed while objects
created by that provider still exist in the state. Re-add the provider
configuration to destroy module.bla.data.aws_caller_identity.logs, after which
you can remove the provider configuration again.
[0m[0m

```

When trying to add placeholder providers ad explicidly pass the providers I add even more trouble. The passing doesn't seem to take place...

```text
[0m[1mmodule.bla.data.aws_caller_identity.logs: Refreshing state...[0m
[0m[1mmodule.bla.data.aws_caller_identity.management: Refreshing state...[0m
[0m[1mdata.aws_caller_identity.manangement: Refreshing state...[0m
[0m[1mdata.aws_caller_identity.logs: Refreshing state...[0m
[0m[1m[32m
Apply complete! Resources: 0 added, 0 changed, 0 destroyed.[0m
[0m[1m[32m
Outputs:

caller_logs = 275093194211
caller_logs_module = 547246093108
caller_management = 605533250831
caller_management_module = 547246093108[0m

```