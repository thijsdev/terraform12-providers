data "aws_caller_identity" "manangement" {
    provider = aws.management
}

data "aws_caller_identity" "logs" {
    provider = aws.logs
}

output "caller_management" {
  value = data.aws_caller_identity.manangement.account_id
}

output "caller_logs" {
  value = data.aws_caller_identity.logs.account_id
}

output "caller_management_module" {
  value = module.bla.caller_management
}

output "caller_logs_module" {
  value = module.bla.caller_logs
}

module "bla" {
  source = "./bla"
#   providers = {
#       logs = "aws.logs"
#       management = "aws.management"
#     }
}
