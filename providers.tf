provider "aws" {
  alias  = "management"
  region = "eu-west-1"

  assume_role {
    role_arn = "arn:aws:iam::605533250831:role/KnabMasterAccess"
  }
}

provider "aws" {
  alias  = "logs"
  region = "eu-west-1"

  assume_role {
    role_arn = "arn:aws:iam::275093194211:role/KnabMasterAccess"
  }
}