data "aws_caller_identity" "management" {
    provider = aws.management
}

data "aws_caller_identity" "logs" {
    provider = aws.logs
}

output "caller_management" {
  value = data.aws_caller_identity.management.account_id
}

output "caller_logs" {
  value = data.aws_caller_identity.logs.account_id
}